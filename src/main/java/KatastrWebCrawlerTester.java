import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by thanak on 2/6/2019.
 */
public class KatastrWebCrawlerTester {

    public static void main(String[] args) {
        test();
    }

    public static void test()
    {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setBinary("c:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
        chromeOptions.addArguments("--headless");

        WebDriver Driver = new ChromeDriver(chromeOptions);
        Driver.navigate().to("https://nahlizenidokn.cuzk.cz/VyberLV.aspx");

        WebDriverWait waitForUsername = new WebDriverWait(Driver, 5000);
        waitForUsername.until(ExpectedConditions.visibilityOfElementLocated(By.id("ctl00_bodyPlaceHolder_vyberObecKU_vyberKU_txtKU")));

        Driver.findElement(By.id("ctl00_bodyPlaceHolder_vyberObecKU_vyberKU_txtKU")).sendKeys("Šenov u Ostravy");
        Driver.findElement(By.id("ctl00_bodyPlaceHolder_vyberObecKU_vyberKU_btnKU")).click();

        WebDriverWait waitForError = new WebDriverWait(Driver, 5000);
        waitForError.until(ExpectedConditions.visibilityOfElementLocated(By.id("ctl00_bodyPlaceHolder_txtLV")));

        Driver.findElement(By.id("ctl00_bodyPlaceHolder_txtLV")).sendKeys("127");

        Driver.findElement(By.id("ctl00_bodyPlaceHolder_btnVyhledat")).click();

        // Take a screenshot of the current page
        File screenshot = ((TakesScreenshot) Driver).getScreenshotAs(OutputType.FILE);

        try {
            FileUtils.copyFile(screenshot, new File("c:/Temp/screenshot.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<WebElement> webElementList = Driver.findElements(By.tagName("table"));

        if(webElementList.size() > 1){
            System.out.println(webElementList.get(1).findElement(By.tagName("tbody")).getText());
        }
        Driver.quit();
    }

}
